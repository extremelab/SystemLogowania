<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width-device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--        <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
        <title></title>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a data-target="#myModalSingIn" data-toggle="modal">Sing Up</a></li>
                        <li><a data-target="#myModalLogIn" data-toggle="modal">Log In</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="modal fade" id="myModalLogIn" role="dialog" style="height: 350px;">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: blue; color: white;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Logowanie</h4>
                    </div>
                    <form action="login.php" method="post">
                        <div class="modal-body">
                            <label>Login</label>
                            <input type="text" class="form-control" name="LoginN"/>
                            <label>Password</label>
                            <input type="password" class="form-control" name="LoginP"/>
                        </div>
                        <div class="modal-footer">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Zaloguj się</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModalSingIn" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: blue; color: white;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Rejestracja</h4>
                    </div>
                    <form action="register.php" method="post">
                        <div class="modal-body">
                            <label>Imię</label>
                            <input type="text" class="form-control" name="Imie"/>
                            <label>Nazwisko</label>
                            <input type="password" class="form-control" name="Nazwisko"/>
                            <label>Adres e-mail</label>
                            <input type="email" class="form-control" name="Email"/>
                            <label>Hasło</label>
                            <input type="password" class="form-control" name="Haslo"/>
                            <label>Powtórz hasło</label>
                            <input type="password" class="form-control" name="HasloP"/>
                        </div>
                        <div class="modal-footer">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Zarejestruj się</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

