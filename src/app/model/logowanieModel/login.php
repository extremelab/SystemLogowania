<?php

$email = $mysqli->escape_string($_POST['email']);
$result = $mysqli->query("SELECT * FROM users WHERE email='$email'");

if($result->num_rows == 0){
    $_SESSION['message'] = "Użytownik z tym mailem nie istnieje";
    header("location: error.php");
}
 else {
    $user = $result->fetch_assoc();
    if(password_verify($_POST['password'], $user['password']))
    {
        $_SESSION['first_name'] = $user['first_name'];
        $_SESSION['last_name'] = $user['last_name'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['active'] = $user['active'];
        $_SESSION['logged_in'] = TRUE;
    }
    else{
        $_SESSION['message'] = "Podane dane są nieprawidłowe";
        header('location: error.php');
    }
}