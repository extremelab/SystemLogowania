<?php

class ConnectionDB implements IConnectionWithDB
{
    private $serverName;
    private $userName;
    private $password;
    private $localhost;
    private $port;
    private $pdo;

    
    public function __construct(string $serverName, string $userName, string $password, string $localhost, string $port='3306') {
        $this->serverName = $serverName;
        $this->userName = $userName;
        $this->password = $password;
        $this->localhost = $localhost;
        $this->port =$port;
    }

    public function ConnectToDB() {
        try{
            $this->pdo = new PDO('mysql:host='.$localhost.';dbname='.$serverName.';port='.$port, $userName, $password );
        }
        catch(PDOException $ex){
            echo 'Połączenie nie mogło zostać utworzone: ' . $ex->getMessage();
        }
    }

    public function GetDBObject(string $query) : mysqli_result
    {
        return $this->pdo->query($query) or die($mysqli->error());
    }

    public function PutDBObject(string $query) {
        $mysqli->query($query) or die($mysqli->error());
        
    }

}